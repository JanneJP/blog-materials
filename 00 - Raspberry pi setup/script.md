# Raspberry pi setup

## Summary

## Requirements

Raspberry pi device

Micro-SD card (>16GB recommended)

Ethernet and power cable

## Prep

Download the [Raspbian](https://www.raspberrypi.org/downloads/raspbian/) image

Burn it in to the SD-card with [BalenaEtcher](https://www.balena.io/etcher/)

Create empty file "ssh" in the boot partition to enable ssh connections.

Insert the SD-card and plug in the cables.

Find the device IP address from your local DHCP server, most likely your router.

Connect to the device via the IP address and port 22 with [Putty](https://putty.org/)

## Setup

### Update

Fetch and install possible updates to firmware etc.

    sudo apt-get update

    sudo apt-get upgrade

### Raspi settings

Skip if not using raspbian. The rest of the guide should work for debian.

    sudo raspi-config

Update the tool with "8 Update"

Change pi user password with "1 Change user password"

Set Timezone, WLAN country and locale from "4 Localisation options"

Expand the filesystem and set video memory split from "7 Advanced options"

Finish and reboot

## Hostname

    sudo nano /etc/hosts

Edit the last line

    sudo nano /etc/hostname

Replace with the same hostname

Reboot

    sudo reboot

## Static IP address

    sudo nano /etc/dhcpcd.conf

Add following to bottom and edit as needed

    interface eth0
    static ip_address=192.168.1.x/24
    static routers=192.168.1.1
    static domain_name_servers=192.168.1.1

## SSH

Change SSH port

    sudo nano /etc/ssh/sshd_config

Uncomment and edit following line:

    #Port 22

## Key auth

Generate keys with puttygen

    mkdir .ssh
    cd .ssh
    nano authorized_keys

Paste in the public key

Set permissions

    chmod 600 authorized_keys
    cd ..
    chmod 700 .ssh

Reboot and test that the keys work

sudo reboot

Disable password login

    sudo nano /etc/ssh/sshd_config

Change line 

    #PasswordAuthentication yes

To

    PasswordAuthentication no

Reboot

    sudo reboot